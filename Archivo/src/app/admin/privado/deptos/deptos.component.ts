import { Component, OnInit } from '@angular/core';
import {DeptosService} from '../../../services/deptos.service';
import {Precio} from '../../../interfaces/interfaces';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-deptos',
  templateUrl: './deptos.component.html',
  styleUrls: ['./deptos.component.css']
})

export class DeptosComponent implements OnInit {
  _http: any;
  tempora:any={};
  alojam:any[]=[];
  activo:any={};
  resss:any;
  costo:any[]=[];
  person:any={};
  active:any={};
  alerta:string;
  lisPre:any[]=[];
  lista:any[]=[];
  user:Precio={};
  users:Precio[]=[];
  limpio:any={};
  ArrPrecio:any={};
  precioAct:any={};
  constructor(public deptoSer:DeptosService, private rou:ActivatedRoute) {

    let id:number= this.rou.snapshot.params['id'];
    console.log(id);
    console.log(this.deptoSer);
    this.deptoSer.getAll().then(res=>{
      this.lista = this.deptoSer.todo;
      console.log(this.lista);
    });
  }

  ngOnInit() {
      console.log(this.deptoSer.alojamiento);
      console.log(this.activo);
      console.log(this.precioAct.id);
  }

  activar(depto){
    this.activo = depto;
    if(this.activo.preper.length==0 && this.activo.capacidad>0){
      for(let i=0;i<this.activo.capacidad;i++){
          this.activo.capacidad[i];
          console.log(this.activo.capacidad[i]);
          let precio = {personas:(i+1),costo:0,id:0,fkdepto:this.activo.id,temporada:''};
          this.activo.preper.push(precio);
          this.ArrPrecio=precio;
          console.log(this.activo);
          console.log(precio);
      }
    }
  }

  AgregarPersona(){
    if (this.activo.personas<=this.activo.capacidad) {
      this.deptoSer.addprecioper({
        fkdepto:this.activo.id,
        temporada:this.activo.temporada,
        personas:this.activo.personas,
        costo:this.activo.costo,
      }).then(res=>{
          console.log(res);
          this.activo.preper.push(this.activo);
          console.log(this.user);
          this.alerta="Persona asignada";
      }, error=>{
          this.alerta = "Ocurrio un error al asignar personsda";
      })
    } else {
            this.alerta = "Execedio en el Numero de persona";
    }
    }

 ActualizarPersona(){
     this.deptoSer.udpprecioper(this.precioAct).then(res=>{
       this.alerta="Precio Actualizado";
        this.activo.costo=this.precioAct.costo;
        this.activo.personas= this.precioAct.personas;
        this.activo.temporada=this.precioAct.temporada;
       console.log(this.precioAct);
     },error=>{
       this.alerta = "Ocurrio un error al actualizar el precio";
     })
   }



  borrar(inp){
    this.precioAct = this.activo.preper[inp];
    this.deptoSer.elimPrecio(this.precioAct).then(res=>{
        this.activo.preper.splice(inp,1);
      console.log(res);
      console.log("eliminada");
    })
}

//sandra navarrete

editar(inp){
  console.log(this.activo.preper[inp]);
  this.precioAct = this.activo.preper[inp];
}

limpiar (){
  this.user.temporada="";
  this.user.costo="";
  this.user.personas="";
}

  cambia(campo,event){
    if(this.activo){
      this.deptoSer.updateVal(this.activo.id,campo,event.target.value).then(success=>{
        this.alerta = "Campo actualizado";
      },error=>{
        this.alerta = "No se pudo realizar la actualizacion";
      });
    }
  }

  disaler(){
    this.alerta=null;
  }

  cambiapre(preper,event,ind){
    console.log(preper);
    console.log(preper,event.target.value);
    console.log("hola")
    //Cambia el valor por persona en la lista de precios.
    if(preper.id>0 && event.target.value>0){
    preper.costo = event.target.value;
    preper.temporada = event.target.value;
    preper.personas = event.target.value;
    console.log(preper.costo);
     this.deptoSer.addprecioper(preper).then(res=>{
       this.activo.preper[ind].id=res['Id'];
       this.activo.preper[ind].costo=preper.costo;
       this.activo.preper[ind].personas=preper.personas;
       this.activo.preper[ind].tempora=preper.temporada;
       console.log("DOS nuebos"+preper);
       console.log("DOS PRECIO"+res)
       this.alerta="Precio asignado";
     },error=>{
       this.alerta = "Ocurrio un error al asignar el precio";
     })
   }
   else{
     if(event.target.value>0){
       preper.costo = Number(event.target.value);
       this.deptoSer.udpprecioper(preper).then(res=>{
         this.alerta="Precio Actualizado";
         this.activo.preper[ind].costo=preper.costo;
         this.activo.preper[ind].personas=preper.personas;
         this.activo.preper[ind].tempora=preper.temporada;
       },error=>{
         this.alerta = "Ocurrio un error al actualizar el precio";
       })
     }else{
       this.alerta="Es necesario especificar un valor";
     }
   }
  }
  // cambiapree(preper,event,ind){
  //   console.log(preper);
  //   console.log(preper,event.target.value);
  //   console.log("hola")
  //   //Cambia el valor por persona en la lista de precios.
  //   if(preper.id==0 && event.target.value>0){
  //   preper.persona = event.target.value;
  //    this.deptoSer.addprecioper(preper).then(res=>{
  //      this.active.preper[ind].id=res['Id'];
  //      this.active.preper[ind].costo=preper.costo;
  //      console.log("DOS nuebos"+preper);
  //      console.log("DOS PRECIO"+res)
  //      this.alerta="Precio asignado";
  //    },error=>{
  //      this.alerta = "Ocurrio un error al asignar el precio";
  //    })
  //  }
  //  else{
  //    if(event.target.value>0){
  //      preper.costo = Number(event.target.value);
  //      this.deptoSer.udpprecioper(preper).then(res=>{
  //        this.alerta="Precio Actualizado";
  //        this.active.preper[ind].costo=preper.costo;
  //      },error=>{
  //        this.alerta = "Ocurrio un error al actualizar el precio";
  //      })
  //    }else{
  //      this.alerta="Es necesario especificar un valor";
  //    }
  //  }
  // }


onSubmit( form: NgForm ){
    if(form.invalid){ console.log("error");return; }
  console.log(this.user);
  console.log(form);
}




//   cambiapersona(preper,event,ind){
//     console.log(preper);
//     console.log("hola");
//     //Cambia el valor por persona en la lista de precios.
// if(preper.id>=0 && event.target.value>0){
// preper.personas=event.target.value;
//      this.deptoSer.addprecioper(preper).then(res=>{
//        this.activo.preper[ind].id=res['Id']
//       this.activo.preper[ind].personas=preper.personas
//       this.activo.preper[ind].temporada=preper.temporada;
//       this.activo.preper[ind].costo=preper.costo;
//        console.log("DOS nuebos"+preper);
//        console.log("DOS PRECIO"+res)
//        this.alerta="Persona asignada";
//      },error=>{
//        this.alerta = "Ocurrio un error al asignar persona";
//      })
//    }
//  }
   // else{
   //   if(event.target.value>0){
   //     preper.costo = Number(event.target.value);
   //     this.deptoSer.udpprecioper(preper).then(res=>{
   //       this.alerta="Precio Actualizado";
   //       this.activo.preper[ind].costo=preper.costo;
   //     },error=>{
   //       this.alerta = "Ocurrio un error al actualizar el precio";
   //     })
   //   }else{
   //     this.alerta="Es necesario especificar un valor";
   //   }
   // }






vers(tipo){
  switch(tipo){
    case "Renta":
    this.lista = this.deptoSer.todo.filter((item)=>item.tipo=='Renta');
    break;
    case "Venta":
    this.lista = this.deptoSer.todo.filter((item)=>item.tipo=='Venta');
    break;
    case "Todo":
    this.lista = this.deptoSer.todo;
    break;
  }
}

busca(event){
  console.log(event.target.value);
  this.lista = this.deptoSer.todo.filter((item)=>item.titulo.indexOf(event.target.value) != -1);
}

}
