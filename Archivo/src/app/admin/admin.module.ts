import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Admin_ROUTING} from './admin.routes';

import {FormsModule} from '@angular/forms';

import { AccesoComponent } from './acceso/acceso.component';
import { DeptosComponent } from './privado/deptos/deptos.component';
import { ReservasComponent } from './privado/reservas/reservas.component';
import { RentasComponent } from './privado/rentas/rentas.component';
import { CatalogosComponent } from './privado/catalogos/catalogos.component';
//import { BarraComponent } from './privado/barra/barra.component';

@NgModule({
  imports: [
    CommonModule,
    Admin_ROUTING,
    FormsModule
  ],
  declarations: [
    AccesoComponent,
    DeptosComponent,
    ReservasComponent,
    RentasComponent,
    CatalogosComponent,
    //BarraComponent
  ]
})
export class AdminModule { }
