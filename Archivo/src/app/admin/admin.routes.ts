import { RouterModule,Routes } from '@angular/router';
import {DeptosComponent} from './privado/deptos/deptos.component';
import {RentasComponent} from './privado/rentas/rentas.component';
import {ReservasComponent} from './privado/reservas/reservas.component';
const Admin_ROUTES:Routes=[

{path:'deptos',component:DeptosComponent},
{path:'reserva',component:ReservasComponent},
{path:'graph',component:RentasComponent},
{path:'**',pathMatch:'full',redirectTo:'login'}
];
export const Admin_ROUTING =RouterModule.forChild(Admin_ROUTES);
