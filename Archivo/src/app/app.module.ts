import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule}  from "@angular/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';

import {APP_ROUTING} from './app.routes';
import { AppComponent } from './app.component';
import { NavbarComponent } from './cliente/shared/navbar/navbar.component';
import { FootComponent } from './cliente/shared/foot/foot.component';
import { BarraComponent } from './admin/privado/barra/barra.component';
import {RegisterComponent} from './cliente/register/register.component';
import {LoginComponent} from './cliente/login/login.component';
//providers

import {DeptosService} from './services/deptos.service';
import {LugaresService} from './services/lugares.service';
import {ProductosService} from './services/productos.service';
import {AccesoService} from './services/acceso.service';
import {AccesoemailService} from './services/accesoemail.service';
import {AccesofaceService} from './services/accesoface.service';
import {AdminComponent} from './admin/admin.component';
import { ClienteComponent } from './cliente/cliente.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
// import { AngularFireAuthModule } from 'angularfire2/auth';

export const firebaseConfig =  {
      apiKey: "AIzaSyDR4LwZCeHds_fSN7bGaCQhqvAQuG0QeeU",
      authDomain: "registro-app-6f3f0.firebaseapp.com",
      databaseURL: "https://registro-app-6f3f0.firebaseio.com",
      projectId: "registro-app-6f3f0",
      storageBucket: "",
      messagingSenderId: "976248315803",
      appId: "1:976248315803:web:af1488b6f593a071"
    }


@NgModule({
  entryComponents:[
    RegisterComponent
  ],
  declarations: [
    AppComponent,
    AdminComponent,
    ClienteComponent,
    NavbarComponent,
    FootComponent,
    BarraComponent,
    RegisterComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
   AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
   AngularFireDatabaseModule,
   AngularFireStorageModule // imports firebase/storage only needed for storage features
  ],
  providers: [DeptosService,LugaresService,ProductosService,AccesoService,AccesofaceService,AccesoemailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
