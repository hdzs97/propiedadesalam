export interface Acceso{
  id?:number;
  idfacebook?:string;
  idgoogle?:string;
  nombre?:string;
  apellidop?:string;
  apellidom?:string;
  correo?:string;
  politicas?:boolean;
  password?:string;
  imagen?:string;
  telefono?:string;
  email?:string;
}

export interface Depto{
  id?:number;
  titulo?:string;
}

export interface Precio {
  id?:number;
  fkdepto?:number;
  personas?:string;
  costo?:string;
  temporada?:string;
}
