import { TestBed, inject } from '@angular/core/testing';

import { AccesoemailService } from './accesoemail.service';

describe('AccesoemailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccesoemailService]
    });
  });

  it('should be created', inject([AccesoemailService], (service: AccesoemailService) => {
    expect(service).toBeTruthy();
  }));
});
