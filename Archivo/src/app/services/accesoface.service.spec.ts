import { TestBed, inject } from '@angular/core/testing';

import { AccesofaceService } from './accesoface.service';

describe('AccesofaceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccesofaceService]
    });
  });

  it('should be created', inject([AccesofaceService], (service: AccesofaceService) => {
    expect(service).toBeTruthy();
  }));
});
