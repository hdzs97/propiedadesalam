import { Injectable } from '@angular/core';
import {Http,Headers} from '@angular/http';

@Injectable()
export class DeptosService {
  urlprincipal:string='https://propiedadeslaureth.com/Api/Deptos/';
  urlApi:string='https://propiedadeslaureth.com/Api/';
  urlPrec:string='https://propiedadeslaureth.com/Api/Precios/';
  urlAcces:string='https://propiedadeslaureth.com/Api/Acceso/';
  alojamiento:any[]=[];
  hoy:any[]=[];
  todo:any[]=[];
  pre:any[]=[];
  ventas:any[]=[];
  alert:string="";
  desde:Date;
  hasta:Date;
  login:any[]=[];
  huesped:number;
  activo:any={};
  cambiapre:any={};
  constructor(private http:Http) { }

  filtrar(desde,hasta,personas,tipo){
    return new Promise((resolve,reject)=>{
      this.http.get(this.urlprincipal+"lista.php?desde="+desde+"&hasta="+hasta+"&huesped="+personas+"&tipo="+tipo).subscribe(reason=>{
        if(reason.status==200){
          let datos= reason.json();
          if(!datos['Error']){
            this.alojamiento = datos;
            console.log(this.alojamiento);
            resolve();
          }else{
            this.alert = datos['Error'];
            reject();
          }
        }else{
          this.alert = "Error en la peticion " + reason.statusText;
          reject();
        }
      });
    })
  }

  getHoy(){
    this.http.get(this.urlprincipal+"limit.php").subscribe(reason=>{
      if(reason.status==200){
        let datos= reason.json();
        if(!datos['Error']){
          this.hoy = datos;
          console.log(this.hoy)
        }else{
          this.alert = datos['Error'];
        }
      }else{
        this.alert = "Error en la peticion " + reason.statusText;
      }
    });
  }

  getVentas(zona){
    let promesa = new Promise((resolve,reject)=>{
      this.http.get(this.urlprincipal+"lista.php?tipo=Venta&zona="+zona).subscribe(reason=>{
        if(reason.status==200){
          let datos= reason.json();
          if(!datos['Error']){
            this.ventas = datos;
            resolve();
          }else{
            this.alert = datos['Error'];
            reject();
          }
        }else{
          this.alert = "Error en la peticion " + reason.statusText;
          reject();
        }
      });
    })
  return promesa;
  }

getAll(){
  return new Promise((resolve,reject)=>{
    this.http.get(this.urlprincipal+"todo.php").subscribe(reason=>{
      if(reason.status==200){
        let datos= reason.json();
        if(!datos['Error']){
          this.todo = datos;
          console.log(this.todo)
          resolve();
        }else{
          this.alert = datos['Error'];
          reject();
        }
      }else{
        this.alert = "Error en la peticion " + reason.statusText;
        reject();
      }
    });
  })
}

updateVal(id,campo,valor){
  return new Promise((resolve,reject)=>{
    this.http.get(this.urlprincipal+"updateval.php?id="+id+"&campo="+campo+"&valor="+valor).subscribe(response=>{
      if(response.status==200){
        let data = response.json();
        if(!data['Error']){
          resolve(data);
        }else{
          reject();
        }
      }else{
        this.alert="Error al realizar la petición";
        reject();
      }
    })
  })
}

addprecioper(data){
  return new Promise((resolve,reject)=>{
    this.http.post(this.urlApi+"Precios/add.php",data).subscribe(response=>{
      if(response.status==200){
        let data = response.json();
        if(!data['Error']){
          console.log(data);
          resolve(data);
        }else{
          reject();
        }
      }else{
        reject();
      }
    })
  })
}


elimPrecio(data){
  return new Promise((resolve,reject)=>{
    this.http.post(this.urlApi+"Precios/elimin.php",data).subscribe(response=>{
      if(response.status==200){
        let data = response.json();
        if(!data['Error']){
          resolve(data);
        }else{
          reject();
        }
      }else{
        reject();
      }
    })
  })
}


udpprecioper(data){
  return new Promise((resolve,reject)=>{
    this.http.post(this.urlApi+"Precios/udp.php",data).subscribe(response=>{
      if(response.status==200){
        let data = response.json();
        if(!data['Error']){
          resolve(data);
        }else{
          reject();
        }
      }else{
        reject();
      }
    })
  })
}


}
