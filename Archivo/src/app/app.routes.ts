import { RouterModule,Routes } from '@angular/router';
import {AdminComponent} from './admin/admin.component';
import {ClienteComponent} from './cliente/cliente.component';
const APP_ROUTES:Routes=[
  {
    path:'admin',
    component:AdminComponent,
    //canActivate:[LoginGuardGuard],
    loadChildren:'./admin/admin.module#AdminModule'
  },
{
  path:'',
  component:ClienteComponent,
  loadChildren:'./cliente/cliente.module#ClienteModule'},
{
  path:'**',
  pathMatch:'full',
  redirectTo:'login'
}
];
export const APP_ROUTING =RouterModule.forRoot(APP_ROUTES,{useHash:true});
