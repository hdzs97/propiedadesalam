import { Component, OnInit } from '@angular/core';
// import {MatFormFieldModule} from '@angular/material/form-field';
import {DeptosService} from '../../services/deptos.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {
  lista: any[]=[];
  zona:string="Dorada";
  constructor(public deptoSer:DeptosService, private rou:Router) {
         this.deptoSer.getVentas(this.zona);
   }

  ngOnInit() {
    this.lista = this.deptoSer.todo;
  }

  casos(zona){

    this.deptoSer.getVentas(zona);

    // switch(zona){
    //   case "Dorada":
    //   this.lista = this.deptoSer.todo.filter((item)=>item.zona=='Dorada');
    //   break;
    //   case "Diamante":
    //   this.lista = this.deptoSer.todo.filter((item)=>item.zona=='Diamante');
    //   break;
    // }
  }

  busca(event){
    console.log("hola")
    console.log(event.target.value);
    this.lista = this.deptoSer.todo.filter((item)=>item.titulo.indexOf(event.target.value) != -1);
  }
  irdepto(depto){
    this.deptoSer.activo=depto;
    this.rou.navigate(['/depto',this.deptoSer.activo.id]);
  }
}
