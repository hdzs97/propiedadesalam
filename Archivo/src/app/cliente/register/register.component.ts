import {Component, OnInit} from '@angular/core';
import{AccesoService} from '../../services/acceso.service';
import {Acceso} from '../../interfaces/interfaces';
import {Validators, FormControl, FormGroup, NgForm} from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import Swal from 'sweetalert2';
import {auth} from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public hola:string
  recordar = false;
  forma:FormGroup;
  user:Acceso={politicas:true}
  maximo:FormGroup;
  password:string;
  pass:string;
  alerta:string;
  mensaje:string;
  Contra
  validCorreo
  constructor(public acces:AccesoService,
              private afAuth:AngularFireAuth) {
   }

   Facebook()  {
       this.afAuth.auth
         .signInWithPopup(new auth.FacebookAuthProvider())
         .then(res =>{
           console.log(res.user);
           let arre=res.user;
           arre.displayName
           console.log(arre.displayName)
           this.acces.registerFace({
         nombre:arre.displayName,
         correo:arre.email,
         imagen:arre.photoURL,
         uid:arre.uid,
       }).then(succ=>{
         Swal.fire({
   position: 'top-end',
   type: 'success',
   title: 'Bienvenido'+arre.displayName,
   showConfirmButton: false,
   timer: 1500
 })
   console.log("Bienvenido",succ);
         console.log("usuario registrado ");
   this.hola = this.acces.mensajes="Bienvenido:"+arre.displayName;

       },err=>
     {
       Swal.fire({
         allowOutsideClick: false,
         type: 'error',
         title:'El usuario ya existe' +err,
       });
       console.log(err);
     })
 });
     }

     Gmail() {
         this.afAuth.auth
           .signInWithPopup(new auth.GoogleAuthProvider())
           .then(res =>{
             console.log(res.user);
             let arre=res.user;
             arre.displayName
             console.log(arre.displayName)
             this.acces.registerEmail({
              nombre:arre.displayName,
              correo:arre.email,
              imagen:arre.photoURL,
              uid:arre.uid,
         }).then(succ=>{
            Swal.fire({
             position: 'top-end',
             type: 'success',
             title: 'Bienvenido'+arre.displayName,
             showConfirmButton: false,
             timer: 1500
           })
                  console.log("Bienvenido",succ);
                  console.log("usuario registrado ");
                  this.hola = this.acces.mensajes="Bienvenido:"+arre.displayName;
                },err=>
            {
              Swal.fire({
                allowOutsideClick: false,
                type: 'error',
                title:'El usuario ya existe' +err,
         });
              console.log(err);
            })
          });
       }


  ngOnInit(){
  }
limpiar (){
  this.user.nombre="";
  this.user.apellidop="";
  this.user.apellidom="";
  this.user.correo="";
  this.user.password="";
  this.pass="";
  this.user.telefono="";
}

comprobar (event){
  console.log(event);
  if(this.pass === this.user.password)
  {
    console.log("iguales");
    this.alerta="correcto";
    this.mensaje="La contraseña son Correcta";
  }
  else {this.alerta ="incorrecto"; console.log("diferentes")
this.mensaje="La contraseña no coincide, intente de nuevo";}

}

// contra(){ (blur)="comprobar($event)" [ngClass]="{'is-invalid':!comprobar,'is-valid':comprobar}"
//  var m=document.getElementById("this.password")
//    var expreg= new RegExp("^(?=(?:.*\d){2})(?=(?:.*[A-Z]){2})(?=(?:.*[a-z]){2})\S{8,}$");
//       console.log(expreg);
//         if(expreg){
//           this.alerta ="contraseña valida.";  this.Contra= true;
//          console.log("contraseña valida.");
//         }else{
//          this.alerta ="debe contener mas caracteres.";
//          console.log("mas caracteres");
//        this.Contra=false;      }}

// validcorreo(event){(blur)="validcorreo($event)" [ngClass]="{ 'is-invalid': !validCorreo,'is-valid':validCorreo }"
//     console.log(event);
//     if(event.target.value){
//       let i = event.target.value.indexOf("@");
//       if(i<0){
//         this.alerta ="El correo debe de ser un correo valido.";
//         this.validCorreo=false;
//       }else{
//       this.validCorreo= true;
//       }
//     }else { this.validCorreo= false;}
// }




actual (){
console.log(this.user);
}

nuev(acitvo){
  console.log(acitvo);
}


  registrar(){
    if(this.pass === this.user.password){
      this.acces.register(this.user).then(succ=>{
        Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Agregado correctamente',
  showConfirmButton: false,
  timer: 1500
})
        console.log("usuario registrado ");
        console.log(this.user);

      },err=>
    {
      Swal.fire({
        allowOutsideClick: false,
        type: 'error',
        title:err,
      });
      console.log(err);
      })
    }
    console.log("name");
    console.log(this.user.nombre);

    if(this.recordar){
      localStorage.setItem('correo',this.user.correo);
    }
      this.hola = this.acces.mensajes="Bienvenido:"+this.user.nombre;
  }

  onSubmit( form: NgForm ){
    if(form.invalid){ console.log("error");return; }
    console.log("Acceso al Sistema");
    console.log(form);
  }

}
