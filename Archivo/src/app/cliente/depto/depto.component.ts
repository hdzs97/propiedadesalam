import { Component, OnInit } from '@angular/core';
import {DeptosService} from '../../services/deptos.service';
import { ActivatedRoute,Router } from "@angular/router";

@Component({
  selector: 'app-depto',
  templateUrl: './depto.component.html',
  styleUrls: ['./depto.component.css']
})

export class DeptoComponent implements OnInit {
  msj: string;
  public depto:any={};
  preci:any[]=[];
  llegada:Date;
  salida:Date;
  huesped:number;
  total:number;
  constructor(public deptoSer:DeptosService,private actro:ActivatedRoute,private rou:Router) {
    // <td>{{(pre.temporada=='Baja')?pre.costo:0}}</td>
    // <td>{{(pre.temporada=='Media')?pre.costo:0}}</td>
    //   <td>{{(pre.temporada=='Alta')?pre.costo:0}}</td>
    // <td>{{(pre.temporada=='Mes')?pre.costo:0}}</td>
    
    let id:number= this.actro.snapshot.params['id'];
    console.log(id);
    console.log(this.deptoSer);

    if(!this.deptoSer.activo.id){
      console.log("activo",+this.deptoSer.alojamiento);
      this.deptoSer.filtrar('','','','Renta').then(()=>{
        this.deptoSer.alojamiento.forEach(item=>{
          if(item.id===id){
            //console.log("existe");
            this.depto=item;
            this.deptoSer.activo=item;
          }
        })
      }).catch(()=>{
        this.rou.navigate(['/alojamiento']);
      })

    }else{
      this.llegada = this.deptoSer.desde;
      this.salida = this.deptoSer.hasta;
      this.huesped = this.deptoSer.huesped;
      this.depto = this.deptoSer.activo;
    }

    if(this.salida && this.llegada && this.huesped && this.depto){
    this.calculaCosto();
    }
  }
// change blur
  ngOnInit() {


    console.log(this.deptoSer.alojamiento);
  }

  calculaCosto(){
    console.log(this.depto);
    if(this.salida && this.llegada && this.huesped && this.depto){
      console.log(this.depto)
      console.log(this.depto.precios)
      console.log(this.huesped)
let capacidad = Number(this.depto.capacidad)
console.log(capacidad)

      let precio:number = Number(this.depto.precio);
      this.depto.precios.forEach(item => {
        if(this.huesped<=capacidad){ if(item.personas==this.huesped){
          precio = precio + Number(item.costo);}}
          else { console.log("hola")
this.msj="Excedio en el numero de personas";
}
      });
      console.log(precio);
      let dif:number = this.diff(this.llegada,this.salida);
      this.total = dif * precio;

      // if(this.huesped>personas){
      //   let diferencia = Number(this.huesped - personas);
      //   let huesp=Number(this.huesped);
      //   let dif = this.diff(this.llegada,this.salida);
      //
      //   for(let i=0;i<this.depto.precios.length;i++){
      //        let prec= {personas:(i+1),costo:0,id:0};
      //        return prec;
      //   }
      //   console.log("hp");
      // //console.log(prec)
      //   let subtotal = dif * precio;
      //   let cuotatotal = (dif * diferencia)*precio;
      //   this.total = subtotal + cuotatotal;
      //   console.log(dif,subtotal,diferencia,personas,cuotatotal,precio,this.total);
      // }
      // else{
      //   let dif = this.diff(this.llegada,this.salida);
      //   let subtotal = dif * precio;
      //   this.total = subtotal;
      //   console.log(dif,subtotal,this.total,precio);
      // }
    }
  }

  diff(date1:Date,date2:Date){
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
  }
}
