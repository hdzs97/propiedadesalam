import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpModule}  from "@angular/http";

import {Cliente_ROUTING} from './cliente.routes';


import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';

import { IndexComponent } from './index/index.component';
// import { NavbarComponent } from './shared/navbar/navbar.component';
// import { FootComponent } from './shared/foot/foot.component';
import { AlojamientoComponent } from './alojamiento/alojamiento.component';
import { VentasComponent } from './ventas/ventas.component';
import { TiendaComponent } from './tienda/tienda.component';
import { GuiaComponent } from './guia/guia.component';
//import { RegisterComponent } from './register/register.component';
//import { LoginComponent } from './login/login.component';
import {DeptoComponent} from './depto/depto.component';

@NgModule({
  entryComponents:[
  //  RegisterComponent,
  ],
  imports: [
    CommonModule,
    Cliente_ROUTING,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatFormFieldModule,
    FormsModule,
    HttpModule,
  ],
  declarations: [
    IndexComponent,
    // NavbarComponent,
    // FootComponent,
    AlojamientoComponent,
    VentasComponent,
    TiendaComponent,
    GuiaComponent,
    //RegisterComponent,
    //LoginComponent,
    DeptoComponent,
  ],
  exports:[
    // FootComponent,NavbarComponent
  ],

})
export class ClienteModule { }
