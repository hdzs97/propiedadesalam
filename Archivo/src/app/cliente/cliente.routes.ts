import { RouterModule,Routes } from '@angular/router';
import {AlojamientoComponent} from './alojamiento/alojamiento.component';
import {GuiaComponent} from './guia/guia.component';
//import {RegisterComponent} from './register/register.component';
import {IndexComponent} from './index/index.component';
import {DeptoComponent} from './depto/depto.component';
import {VentasComponent} from './ventas/ventas.component';
const Cliente_ROUTES:Routes=[

  {path:'alojamiento',component:AlojamientoComponent},
  {path:'guia',component:GuiaComponent},
  {path:'ventas',component:VentasComponent},
  {path:'depto/:id',component:DeptoComponent},
  {path:'',component:IndexComponent},
  {path:'**',pathMatch:'full',redirectTo:'login'}
];
export const Cliente_ROUTING =RouterModule.forChild(Cliente_ROUTES);
