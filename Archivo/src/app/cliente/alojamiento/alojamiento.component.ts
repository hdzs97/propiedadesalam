import { Component, OnInit } from '@angular/core';
import {DeptosService} from '../../services/deptos.service';
import { ActivatedRoute,Router } from "@angular/router";
@Component({
  selector: 'app-alojamiento',
  templateUrl: './alojamiento.component.html',
  styleUrls: ['./alojamiento.component.css']
})
export class AlojamientoComponent implements OnInit {
  llegada:Date;
  salida:Date;
  huesped:number;
  constructor(public deptoSer:DeptosService,private act:ActivatedRoute,private rou:Router) {
    console.log(this.deptoSer.desde,this.deptoSer.hasta);
    this.llegada = this.deptoSer.desde;
    this.salida = this.deptoSer.hasta;
    this.huesped= this.deptoSer.huesped
    if(this.llegada){
    this.deptoSer.filtrar(this.llegada,this.salida,this.huesped,"Renta");
  }else{
    this.deptoSer.filtrar("","","","Renta");
  }

  }

  ngOnInit() {
  }

  buscar(){
    console.log("ejecutando submit");
    this.deptoSer.filtrar(this.llegada.toISOString().substr(0,10),this.salida,this.huesped,"Renta");
  }

  irdepto(depto){
    this.deptoSer.activo=depto;
    this.rou.navigate(['/depto',this.deptoSer.activo.id]);
  }

}
