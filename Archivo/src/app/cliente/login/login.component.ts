import { Component, OnInit,Input,Output,EventEmitter, HostListener } from '@angular/core';
// import {AccesofaceService} from '../../services/accesoface.service';
import{AccesoService} from '../../services/acceso.service';
import {Acceso} from '../../interfaces/interfaces';
import { NgForm } from '@angular/forms';
import {Validators, FormControl, FormGroup} from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import Swal from 'sweetalert2';
import {auth} from 'firebase';
// import { AccesoemailService } from '../../services/accesoemail.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public hola:string
  modal=true;
  regist:any={};
  nombre:string;
  recordar=false;
  ola;
  user:Acceso={};
  @Input() titulo:string;
  @Output() accion= new EventEmitter();


  constructor(public acces:AccesoService,private afAuth:AngularFireAuth, public router: Router) { }


  mod(tr){
  this.acces.modal = this.modal=tr;
  }

  limpiar (){
    this.user.nombre="";
    this.user.apellidop="";
    this.user.apellidom="";
    this.user.correo="";
    this.user.password="";
    this.user.telefono="";
  }

normal (){
  this.acces.Login(this.user).then(succ=>{
    console.log("bien");
    console.log(this.user);
  },err=>
{
  console.log(err);
    console.log("mal");
  })
}



  Facebook() {
      this.afAuth.auth
        .signInWithPopup(new auth.FacebookAuthProvider())
        .then(res =>{
          console.log(res.user);
          let arre=res.user;
          arre.displayName
          console.log(arre.displayName)
          this.acces.registerFace({
        nombre:arre.displayName,
        correo:arre.email,
        imagen:arre.photoURL,
        uid:arre.uid,
      }).then(succ=>{
        Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Bienvenido'+arre.displayName,
  showConfirmButton: false,
  timer: 1500
})
  console.log("Bienvenido",succ);
        console.log("usuario registrado ");
  this.hola = this.acces.mensajes="Bienvenido:"+arre.displayName;

      },err=>
    {
      Swal.fire({
        allowOutsideClick: false,
        type: 'error',
        title:'El usuario ya existe' +err,
      });
      console.log(err);
    })
});
    }


    Gmail() {
        this.afAuth.auth
          .signInWithPopup(new auth.GoogleAuthProvider())
          .then(res =>{
            console.log(res.user);
            let arre=res.user;
            arre.displayName
            console.log(arre.displayName)
            this.acces.registerEmail({
             nombre:arre.displayName,
             correo:arre.email,
             imagen:arre.photoURL,
             uid:arre.uid,
        }).then(succ=>{
           Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'Bienvenido'+arre.displayName,
            showConfirmButton: false,
            timer: 1500
          })
                 console.log("Bienvenido",succ);
                 console.log("usuario registrado ");
                 this.hola = this.acces.mensajes="Bienvenido:"+arre.displayName;
               },err=>
           {
             Swal.fire({
               allowOutsideClick: false,
               type: 'error',
               title:'El usuario ya existe' +err,
        });
             console.log(err);
           })
         });
      }

  ngOnInit() {
  this.regist=this.acces;
  console.log(this.regist)


    if (localStorage.getItem('correo')) {
      this.user.correo = localStorage.getItem('correo');
      this.recordar=true;
    }
  }

  realizar(event){
      console.log("Esta es una respuesta desde navbar a partir de login "+event.data);
  }

  mdl(opcion){
    console.log(opcion);
    this.modal=opcion;
  }
  //salvar(){
    //console.log("se activo el boton");
    //let cerrar = document.getElementById('cerrar');
    //cerrar.click();
    //this.accion.emit({data:'en login ya termine'});
  //}
  onSubmit( form: NgForm ){
    if(form.invalid){return;}
    console.log("Acceso al Sistema");
    console.log(form);
  }

}
