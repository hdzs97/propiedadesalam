import { Component, OnInit } from '@angular/core';
import {DeptosService} from '../../services/deptos.service';

import {Router,ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  ventas:any[]=[]
  desde:Date;
  hasta:Date;
  huesped:number;
  constructor(public deptoSer:DeptosService,private router:Router,private activatedRoute:ActivatedRoute,) {
    this.deptoSer.getHoy();
    this.deptoSer.getVentas("Dorada").then(()=>{
    this.ventas = this.deptoSer.ventas.slice(0,3);    //console.log(this.ventas);

  },()=>{
    console.log("sin datos para mostrar");
  });
   }

  ngOnInit() {
    //console.log(this.ventas);
  }

  mover(event){
    console.log("esto es una prueba");
  }

  onScroll($event){
    console.log($event);
  }

  disponibilidad(){
    this.deptoSer.desde = this.desde;
    this.deptoSer.hasta = this.hasta;
    this.deptoSer.huesped = this.huesped;
    // this.router.navigate(['/alojamiento']);
  }
  irdepto(depto){
    this.deptoSer.activo=depto;
  }

}
