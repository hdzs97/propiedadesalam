import { Component, OnInit,HostListener } from '@angular/core';
import { AccesoService } from '../../../services/acceso.service';

@Component({
  selector: 'cliente-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  modal=true;
  valido;
  validos;
  var=false;
  dos;
  constructor(public acces:AccesoService) { }

  ngOnInit()
  {
     this.valido="Registrarse"
     this.validos="Acceder";
  }

menu(){
  if (this.acces.mensajes) {
    this.valido="";
    this.validos="";
  }
}

  realizar(event){
      console.log("Esta es una respuesta desde navbar a partir de login "+event.data);
  }

   mdl(opcion){
    console.log(opcion);
    this.acces.modal = this.modal=opcion;
    console.log(this.modal)
  }

  @HostListener('scroll', ['$event'])
  onElementScroll($event) {

    let banner = $event.target.scrollingElement;
    // console.log(banner.scrollTop);
    let menu = document.getElementById("menu");
    if(banner.scrollTop==0){
      if(menu.classList.contains("bg-light")){
        menu.classList.remove("bg-light");
        menu.classList.add("bgtrans");
      }
    }else{
      if(menu.classList.contains("bgtrans")){
        menu.classList.remove("bgtrans");
        menu.classList.add("bg-light");
      }
    }
  }


}
